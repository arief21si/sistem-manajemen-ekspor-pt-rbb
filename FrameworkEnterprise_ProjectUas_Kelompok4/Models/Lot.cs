﻿using System.ComponentModel.DataAnnotations;

namespace FrameworkEnterprise_ProjectUas_Kelompok4.Models;

public class Lot
{
    public int Id { get; set; }
    public double Berat { get; set; }
    public string? Deskripsi { get; set; }
    public bool StatusSortir { get; set; }

    public User? Inventaris { get; set; }
    public User? Agen { get; set; }

    [DataType(DataType.Date)] public DateTime Tanggal { get; set; }
}