﻿namespace FrameworkEnterprise_ProjectUas_Kelompok4.Models.Enums;

public enum HakAksesEnum
{
    Admin = 1,
    Manajer = 2,
    Inventory = 3,
    Agen = 4,
}
