﻿using FrameworkEnterprise_ProjectUas_Kelompok4.Data;
using FrameworkEnterprise_ProjectUas_Kelompok4.Models.Enums;
using Microsoft.EntityFrameworkCore;

namespace FrameworkEnterprise_ProjectUas_Kelompok4.Models;

public class Seeder
{
    public static void Initialize(IServiceProvider serviceProvider)
    {
        using (var context = new DatabaseContext(
                   serviceProvider.GetRequiredService<DbContextOptions<DatabaseContext>>()
               ))
        {
            if (context.User.Any()) return;

            var userManager = new UserManager();

            var Admin = new User
            {
                Nama_lengkap = "Admin PT RBB",
                Username = "adminrbb",
                No_hp = "081312341234",
                HakAkses = HakAksesEnum.Admin
            };

            Admin.Password = userManager.HashPassword(Admin, "password");

            var Manajer_1 = new User
            {
                Nama_lengkap = "Manager 1",
                Username = "manager1",
                No_hp = "081312341234",
                HakAkses = HakAksesEnum.Manajer
            };
            Manajer_1.Password = userManager.HashPassword(Manajer_1, "password");

            var Manajer_2 = new User
            {
                Nama_lengkap = "Manager 2",
                Username = "manager2",
                No_hp = "081312341234",
                HakAkses = HakAksesEnum.Manajer
            };
            Manajer_2.Password = userManager.HashPassword(Manajer_2, "password");

            var Agen_1 = new User
            {
                Nama_lengkap = "Agen 1",
                Username = "agen1",
                No_hp = "081312341234",
                HakAkses = HakAksesEnum.Agen
            };
            Agen_1.Password = userManager.HashPassword(Agen_1, "password");

            var Agen_2 = new User
            {
                Nama_lengkap = "Agen 2",
                Username = "agen2",
                No_hp = "081312341234",
                HakAkses = HakAksesEnum.Agen
            };
            Agen_2.Password = userManager.HashPassword(Agen_2, "password");

            var Agen_3 = new User
            {
                Nama_lengkap = "Agen 3",
                Username = "agen3",
                No_hp = "081312341234",
                HakAkses = HakAksesEnum.Agen
            };
            Agen_3.Password = userManager.HashPassword(Agen_3, "password");

            var Inventory_1 = new User
            {
                Nama_lengkap = "Inventaris 1",
                Username = "inventaris1",
                No_hp = "081312341234",
                HakAkses = HakAksesEnum.Inventory
            };
            Inventory_1.Password = userManager.HashPassword(Inventory_1, "password");

            var Inventory_2 = new User
            {
                Nama_lengkap = "Inventaris 2",
                Username = "inventaris2",
                No_hp = "081312341234",
                HakAkses = HakAksesEnum.Inventory
            };
            Inventory_2.Password = userManager.HashPassword(Inventory_2, "password");

            context.User.AddRange(
                Admin,
                Manajer_1,
                Manajer_2,
                Agen_1,
                Agen_2,
                Agen_3,
                Inventory_1,
                Inventory_2
            );

            context.SaveChanges();
        }
    }
}