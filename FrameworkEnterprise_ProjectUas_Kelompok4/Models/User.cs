﻿using FrameworkEnterprise_ProjectUas_Kelompok4.Models.Enums;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace FrameworkEnterprise_ProjectUas_Kelompok4.Models;

public class User
{
    public int Id { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string Nama_lengkap {  get; set; }
    public string No_hp { get; set; }
    public HakAksesEnum HakAkses { get; set;}
}
