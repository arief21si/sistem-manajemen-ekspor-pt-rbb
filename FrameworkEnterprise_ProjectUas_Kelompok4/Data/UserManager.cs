﻿using FrameworkEnterprise_ProjectUas_Kelompok4.Models;
using Microsoft.AspNetCore.Identity;

namespace FrameworkEnterprise_ProjectUas_Kelompok4.Data;

public class UserManager
{
    private readonly PasswordHasher<User> _passwordHasher;

    public UserManager()
    {
        _passwordHasher = new PasswordHasher<User>();
    }

    public string HashPassword(User user, string password)
    {
        return _passwordHasher.HashPassword(user, password);
    }

    public bool VerifyPassword(User user, string password, string hashedPassword)
    {
        var result = _passwordHasher.VerifyHashedPassword(user, hashedPassword, password);
        return result == PasswordVerificationResult.Success;
    }
}
