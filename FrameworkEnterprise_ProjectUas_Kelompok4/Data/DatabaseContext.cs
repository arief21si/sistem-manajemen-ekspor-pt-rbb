using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FrameworkEnterprise_ProjectUas_Kelompok4.Models;

namespace FrameworkEnterprise_ProjectUas_Kelompok4.Data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext (DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        public DbSet<FrameworkEnterprise_ProjectUas_Kelompok4.Models.User> User { get; set; } = default!;

        public DbSet<FrameworkEnterprise_ProjectUas_Kelompok4.Models.Lot> Lot { get; set; } = default!;
    }
}
