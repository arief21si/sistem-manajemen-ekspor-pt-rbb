using FrameworkEnterprise_ProjectUas_Kelompok4.Data;
using FrameworkEnterprise_ProjectUas_Kelompok4.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FrameworkEnterprise_ProjectUas_Kelompok4.Controllers;

[Authorize]
public class LotController : Controller
{
    private readonly DatabaseContext _context;

    public LotController(DatabaseContext context, HttpContext httpContext)
    {
        _context = context;
    }

    // GET: Lot
    public async Task<IActionResult> Index()
    {
        if (_context.Lot == null)
            Problem("Entity set 'DatabaseContext.Lot'  is null.");

        var lots = await _context.Lot.Include(a => a.Agen).ToListAsync();

        return View(lots);
    }

    // GET: Lot/Details/5
    public async Task<IActionResult> Details(int? id)
    {
        if (id == null || _context.Lot == null) return NotFound();

        var lot = await _context.Lot
            .FirstOrDefaultAsync(m => m.Id == id);
        if (lot == null) return NotFound();

        return View(lot);
    }

    // GET: Lot/Create
    public IActionResult Create()
    {
        return View();
    }

    // POST: Lot/Create
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(
        [Bind("Id,Berat,Deskripsi,StatusSortir,Tanggal")]
        Lot lot)
    {
        if (ModelState.IsValid)
        {
            var claimUser = HttpContext.User;

            // Get ID from claim
            var idUser = claimUser.Claims.FirstOrDefault(c => c.Type == "Id")?.Value;
            if (idUser == null) return NotFound();

            lot.Agen = await _context.User.FindAsync(int.Parse(idUser));


            _context.Add(lot);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        return View(lot);
    }

    // GET: Lot/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
        if (id == null || _context.Lot == null) return NotFound();

        var lot = await _context.Lot.FindAsync(id);
        if (lot == null) return NotFound();
        return View(lot);
    }

    // POST: Lot/Edit/5
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id,
        [Bind("Id,Berat,Deskripsi,StatusSortir,Tanggal")]
        Lot lot)
    {
        if (id != lot.Id) return NotFound();


        // return RedirectToAction(nameof(Index));

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(lot);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LotExists(lot.Id))
                    return NotFound();
                throw;
            }

            return RedirectToAction(nameof(Index));
        }

        return View(lot);
    }

    // GET: Lot/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null || _context.Lot == null) return NotFound();

        var lot = await _context.Lot
            .FirstOrDefaultAsync(m => m.Id == id);
        if (lot == null) return NotFound();

        return View(lot);
    }

    // POST: Lot/Delete/5
    [HttpPost]
    [ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        if (_context.Lot == null) return Problem("Entity set 'DatabaseContext.Lot'  is null.");
        var lot = await _context.Lot.FindAsync(id);
        if (lot != null) _context.Lot.Remove(lot);

        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool LotExists(int id)
    {
        return (_context.Lot?.Any(e => e.Id == id)).GetValueOrDefault();
    }
}