﻿using System.Security.Claims;
using FrameworkEnterprise_ProjectUas_Kelompok4.Data;
using FrameworkEnterprise_ProjectUas_Kelompok4.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FrameworkEnterprise_ProjectUas_Kelompok4.Controllers;

public class AuthController : Controller
{
    private readonly DatabaseContext _context;

    public AuthController(DatabaseContext context)
    {
        _context = context;
    }

    public IActionResult Login()
    {
        var claimUser = HttpContext.User;

        if (claimUser.Identity is { IsAuthenticated: true })
            return RedirectToAction("Index", "Home");

        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Login(LoginViewModel loginViewModel)
    {
        var userManager = new UserManager();

        var user = await _context.User.SingleOrDefaultAsync(
            u => u.Username == loginViewModel.Username.Trim());

        if (user == null || !userManager.VerifyPassword(user, loginViewModel.Password.Trim(), user.Password.Trim()))
        {
            ViewData["ValidateMessage"] = "Username atau Password salah";
            return View();
        }

        var claims = new List<Claim>
        {
            new(ClaimTypes.NameIdentifier, loginViewModel.Username),
            new("Nama_lengkap", user.Nama_lengkap),
            new("Id", user.Id.ToString()),
            new("Role", user.HakAkses.ToString())
        };

        var claimsIdentity = new ClaimsIdentity
            (claims, CookieAuthenticationDefaults.AuthenticationScheme);

        var properties = new AuthenticationProperties
        {
            AllowRefresh = true,
            IsPersistent = loginViewModel.RememberMe
        };

        await HttpContext.SignInAsync(
            CookieAuthenticationDefaults.AuthenticationScheme,
            new ClaimsPrincipal(claimsIdentity),
            properties
        );

        return RedirectToAction("Index", "Home");
    }

    public async Task<IActionResult> LogOut()
    {
        await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        return RedirectToAction("Login", "Auth");
    }
}