using Microsoft.AspNetCore.Mvc;

namespace FrameworkEnterprise_ProjectUas_Kelompok4.Controllers;

public class AccountController : Controller
{
    public IActionResult AccessDenied()
    {
        TempData["Error"] = "Anda tidak memiliki akses untuk halaman tersebut";
        return RedirectToAction("Index", "Home");
    }
}